{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Machine learning tutorial at CMSDAS at CERN, June 2024\n",
    "## Exercise 2: binary classification\n",
    "\n",
    "Evan Armstrong Koenig, Matthias Komm, Pietro Vischia\n",
    "\n",
    "The core of this tutorial comes from https://github.com/vischia/data_science_school_igfae2024 (Pietro Vischia (pietro.vischia@cern.ch)).\n",
    "\n",
    "The CMSDAS version extends it to consider a convolutional network to regress Higgs quantities, plus some fixes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Uncomment and run this if you are running on Colab (remove only the \"#\", keep the \"!\").\n",
    "# You can run it anyway, but it will do nothing if you have already installed all dependencies\n",
    "# (and it will take some time to tell you it is not gonna do anything)\n",
    "\n",
    "\n",
    "#from google.colab import drive\n",
    "#drive.mount('/content/drive')\n",
    "#%cd \"/content/drive/MyDrive/\"\n",
    "#! git clone https://github.com/vischia/data_science_school_igfae2024.git\n",
    "#%cd machine_learning_tutorial\n",
    "#!pwd\n",
    "#!ls\n",
    "#!pip install livelossplot shap"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "matplotlib.rcParams['figure.figsize'] = (8, 6)\n",
    "matplotlib.rcParams['axes.labelsize'] = 14\n",
    "\n",
    "import glob\n",
    "import os\n",
    "import re\n",
    "import math\n",
    "import socket\n",
    "import json\n",
    "import pickle\n",
    "import gzip\n",
    "import copy\n",
    "import array\n",
    "import numpy as np\n",
    "import numpy.lib.recfunctions as recfunc\n",
    "from tqdm import tqdm\n",
    "\n",
    "from scipy.optimize import newton\n",
    "from scipy.stats import norm\n",
    "\n",
    "import uproot\n",
    "\n",
    "import datetime\n",
    "from timeit import default_timer as timer\n",
    "\n",
    "import sklearn\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.utils import shuffle\n",
    "from sklearn.metrics import roc_curve, auc, accuracy_score\n",
    "from sklearn.tree import export_graphviz\n",
    "from sklearn.inspection import permutation_importance\n",
    "try:\n",
    "    # See #1137: this allows compatibility for scikit-learn >= 0.24\n",
    "    from sklearn.utils import safe_indexing\n",
    "except ImportError:\n",
    "    from sklearn.utils import _safe_indexing\n",
    "\n",
    "import pandas as pd\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Neural networks\n",
    "\n",
    "#### Details on neural networks\n",
    "\n",
    "Biology teaches us that the brain is constituted of neurons and connections between them: the synapses.\n",
    "By comparing the brain of various animals, we now think that the more the number of neurons and most importantly of synapses is large, the more complex are the functions that the brain can execute.\n",
    "\n",
    "Let's learn the inner workings of a very simplified mathematical model of brain: an artificual neural network.\n",
    "\n",
    "The first element is the neuron. The simplest model (and one of the first) we have is the [*perceptron*](https://en.wikipedia.org/wiki/Perceptron). The neuron is modelled by a mathematical function that takes some arguments as inputs, combines them linearly, and returns an output value.\n",
    "We denote as *weights* the coefficients of the linear combination.\n",
    "\n",
    "However, we want to be able to approximate nonlinear functions, so we need to plug in a degree of nonlinearity inside the neuron, and we want the neuron to fire only when a certain threshold in the output is reached (a certain amount of stimulation).\n",
    "\n",
    "We modify the output of the neuron by an activation function $f_{act}$: the neuron is activated if the activation function returns a non-zero value. The output of the neuron is defined as:\n",
    "\n",
    "$$\n",
    "y_n = f_{act}(\\sum_i w_{i,n} x_{i,n})\n",
    "$$\n",
    "\n",
    "If the activation function is not linear, we are happy because we have obtained a neuron that gives a nonlinear output and gets activated only if the stimuli it receives are large enough.\n",
    "\n",
    "The activation function become larger than 0.5 at $x=0$. We need to include the possibility of shifting the value for which the neuron activates. This is done by introducing a bias. The neuron output, that we wrote above as\n",
    "\n",
    "$$\n",
    "y_n = f_{act}(\\sum_i w_{i,n} x_{i,n})\n",
    "$$\n",
    "\n",
    "(that activates for $\\sum(w_i x_i)>0$) will be\n",
    "\n",
    "$$\n",
    "y_n = f_{act}(\\sum_i w_{i,n} x_{i,n} + w_{bb})\n",
    "$$\n",
    "\n",
    "that activates at a learnable ($w_b$) value. \n",
    "\n",
    "\n",
    "You will use in this exercise mostly two activation functions:\n",
    "\n",
    "- [ReLU](https://en.wikipedia.org/wiki/Rectifier_(neural_networks)), a function $f(x)$ that returns 0 if $x<0$, and $x$ otherwise;\n",
    "- [sigmoid](https://en.wikipedia.org/wiki/Sigmoid_function), a function that rescales any number into a number between 0 and 1.\n",
    "\n",
    "Here you have a graphical representation of the perceptron, by [https://towardsdatascience.com](https://towardsdatascience.com):\n",
    "\n",
    "![neuron](https://miro.medium.com/max/1435/1*n6sJ4yZQzwKL9wnF5wnVNg.png \"Figure from https://towardsdatascience.com\")\n",
    "\n",
    "Now we have to connect the neurons. The simplest way is to build layers of neurons, and to connect all neurons of consecutive layers. Starting with the inputs, there is a first layer of neurons. Each neuron combines linearly the inputs and passes the result through activation function to give an output value. The set of outputs of a layer will be the input of the following layer:\n",
    "\n",
    "![neuralnet](https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Colored_neural_network.svg/800px-Colored_neural_network.svg.png \"Figure from wikipedia\")\n",
    "\n",
    "A neural network is characterized by a set of weights assigned to the connections that define the structure of the network. You can see this as a mathematical function with many free parameters (the weights) that takes the inputs and gives an output. The problem of learning is then the problem of finding the values of the free parameters that minimize the difference between the output and the target distribution that we want to learn.\n",
    "\n",
    "\n",
    "#### The training process\n",
    "\n",
    "Schematically, the training process consists in:\n",
    "\n",
    "- for each epoch\n",
    "   * for each training set data point:\n",
    "      1. calculate the output of each neuron, starting from the inputs to the output\n",
    "      2. compare the output of the last neuron with the reference wine quality\n",
    "      3. propagate the error back towards the inputs, without updating the weights (the error needs to be propagated with respect to the current values of the weights)\n",
    "      4. update all the weights\n",
    "      5. save the value of the loss function for each event\n",
    "   * for each test set data point:\n",
    "      1. save the value of the loss function for each event\n",
    "   * aggregate the errors by computing a Mean Squared Average (MSE)\n",
    "      1. the MSE of the errors in the training dataset events is the average training loss\n",
    "      2. the MSE of the errors in the test dataset is the averate validation loss (you see here I am using validation and test indifferently)\n",
    "\n",
    "The idea is the training will stop when the loss function doesn't improve anymore (it remains stationary at its minimum. If the training loss keeps diminishing and the test loss begins increasing, then we might be starting to learn statistical fluctuations of the training dataset.\n",
    "\n",
    "\n",
    "#### Clarification on the connections between networks (to fix ideas)\n",
    "\n",
    "If the network has the following structure: (input layer: two inputs `A` et `B`;  first internal (_hidden_) layer: two neurons `1a` et `1b`; second hidden layer: two neurons `2a` et `2b`; output `y`), the list of connections (the weights) is:\n",
    "\n",
    "- Four weights connecting the inputs to the layer 1:\n",
    "    - `wA1a` (connects input `A` to neuron `1a`)\n",
    "    - `wA1b` (connects input `A` to neuron `1b`)\n",
    "    - `wB1a` (connects input `B` to neuron `1a`)\n",
    "    - `wB1b` (connects input `B` to neuron `1b`)\n",
    "- Four weights connecting the neurons of layer 1 to those of layer 2:_\n",
    "    - `W1a2a` (connects neuron `1a` to neuron `2a`)\n",
    "    - `W1a2b` (connects neuron `1a` to neuron `2b`)\n",
    "    - `W1b2a` (connects neuron `1b` to neuron `2a`)\n",
    "    - `W1b2b` (connects neuron `1b` to neuron `2b`)\n",
    "- Two weights connecting the neurons of layer 2 to the output y:\n",
    "    - `W2ay` (connects neuron `2a` to output `y`)\n",
    "    - `W2by` (connects neuron `2b` to output `y`)\n",
    "\n",
    "####  Backpropagation\n",
    "\n",
    "To perform backpropagation we need, for each neuron, to propagate back the error of the neurons of the following layer (so you need to go backwards). We use the chain rule.\n",
    "\n",
    "- Error for a neuron of the output layer:\n",
    "\n",
    "$$\n",
    "\\epsilon = (y_{true} - \\hat{y}) * activation\\_derivative(\\hat{y})\n",
    "$$\n",
    "\n",
    "Here $\\hat{y}$ is the output of this output neuron, and $y_{true}$ is the target quality of the wine\n",
    "\n",
    "- Error for a neuron $m$ of an internal layer $N$:\n",
    "\n",
    "$$\n",
    "\\epsilon_{m, N} = \\sum_{k} (w_{k, N+1} * \\epsilon_{k, N+1}) * activation\\_derivative(\\hat{m})\n",
    "$$\n",
    "\n",
    "Here, $\\epsilon_{k,N+1}$ is the error of the neuron $k$ of the following layer (layer $N+1$), $w_{k, N+1}$ is the weight of the connection between the neuron $m$ and the neuron $k$ of the next layer, and $\\hat{m}$ is the output of neuron $m$\n",
    "\n",
    "\n",
    "#### Updating the weights\n",
    "\n",
    "After having backpropagated all the gradient, you have to update all the weights using this formula:\n",
    "\n",
    "$$\n",
    "w = w + learning\\_rate * erreur * input\n",
    "$$\n",
    "\n",
    "Ici $erreur$ is the error calculated via backpropagation, $input$ is the input value of the neuron that had been originally passed to the neuron, and $learning rate$ is a parameter governing how fast we climb down the gradient.\n",
    "\n",
    " \n",
    "#### At the end of each epoch\n",
    "\n",
    "To check for convergence of the network, a standard practice is to aggregeate the errors $\\hat{y} - y_{true}$  of all the events at the end of each epoch, in order to reduce the sensitivity to statistical fluctuations in the training sample. The first pillar of statistical wisdom according to Stigler is precisely aggregation. \n",
    "\n",
    "In analogy with $\\chi^2$ fit, we can for example calculate the $MSE = \\frac{1}{N} \\sum_{events} (\\hat{y}-y_{true})^2$ and plot the MSE as a function of the number of epochs. If the network is improving its predictions, we should see something like this:\n",
    "\n",
    "![mse](https://cern.ch/vischia/mse_pythonCourse.png \"Figure by Pietro Vischia, 2019\")\n",
    "\n",
    "#### Diagnostic plots\n",
    "\n",
    "- 1) MSE as a function of the epoch\n",
    "- 2) Histogram of $\\frac{\\hat{y} - y_{true}}{y_{true}}$ \n",
    "- 3) Histogram of $\\frac{\\hat{y} - y_{true}}{y_{true}}$ as a function of $y_{true}$\n",
    "\n",
    "\n",
    "### Weights initialization\n",
    "- To initialize the weights at the beginning you can use a Gaussian, or a truncated gaussian ( (scipy.stats.truncnorm), or a $random uniform[0,1]$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torchinfo\n",
    "import torch\n",
    "torch.manual_seed(0)\n",
    "\n",
    "import torch.nn as nn\n",
    "from torch.utils.data import Dataset\n",
    "from torch.utils.data import DataLoader\n",
    "\n",
    "import torch.nn.functional as F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's briefly see how `torch`deals with gradients"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's calculate gradients of a simple equation using autodiff"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x0 = torch.tensor(1., requires_grad=True)\n",
    "x1 = torch.tensor(2., requires_grad=True)\n",
    "print(\"x0 = \",x0)\n",
    "print(\"x1 = \",x1)\n",
    "\n",
    "p = 2*x0 + x0*torch.sin(x1) + x1**3\n",
    "print(\"p = \",p)\n",
    "\n",
    "p.backward()\n",
    "print(\"dp/dx0 = \",x0.grad)\n",
    "print(\"dp/dx1 = \", x1.grad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#compare with analytic solution\n",
    "x0_grad_analytic = 2+torch.sin(x1)\n",
    "x1_grad_analytic = x0*torch.cos(x1)+3*x1**2\n",
    "\n",
    "print(\"dp/dx0 = \",x0_grad_analytic)\n",
    "print(\"dp/dx1 = \",x1_grad_analytic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try to redefine the function, but this time we will use the `sin` function from the `math` library instead of the `torch` library.\n",
    "\n",
    "Do you think this will make a difference? Let's try!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = 2*x0 + x0*math.sin(x1) + x1**3\n",
    "print(p)\n",
    "p.backward()\n",
    "print(x0.grad, x1.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is important to use operations that have been overloaded within the library: implement the same equation, but this time the function `sin` is imported from `math`.\n",
    "\n",
    "Notice how no error message is thrown, but gradients are completely different. This is because `torch`is blind to the portion of equation involving `math.sin`, in the sense that it cannot anymore propagate the gradient through it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's plot some activation function.\n",
    "\n",
    "Let's also plot the derivative of each activation function, in two ways: by plotting the explicitly coded derivative function, and by plotting the derivative computed via automatic differentiation, `x.grad`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = torch.tensor(np.linspace(-6, 6, 100), requires_grad=True)\n",
    "y = torch.sigmoid(x)\n",
    "yprime = lambda x: torch.sigmoid(x)*(1-torch.sigmoid(x))\n",
    "\n",
    "func, =plt.plot(x.detach().numpy(),y.detach().numpy(), label=\"sigmoid(x)\")\n",
    "plt.xlabel(\"x\")\n",
    "Y = torch.sum(y)\n",
    "Y.backward()\n",
    "derivative, =plt.plot(x.detach().numpy(),x.grad.detach().numpy(), label=\"autodiff sigmoid'(x)\")\n",
    "anal_derivative = plt.plot(x.detach().numpy(), yprime(x).detach().numpy(), label=\"analytical sigmoid'(x)\", linestyle='--', linewidth=2)\n",
    "\n",
    "plt.legend()\n",
    "plt.show()\n",
    "plt.close()\n",
    "\n",
    "plt.figure()\n",
    "x = torch.tensor(np.linspace(-6, 6, 100), requires_grad=True)\n",
    "y =torch.relu(x)\n",
    "yprime = lambda x: torch.where(x>0, 1,0) \n",
    "\n",
    "func, = plt.plot(x.detach().numpy(),y.detach().numpy(), label=\"relu(x)\")\n",
    "plt.xlabel(\"x\")\n",
    "Y = torch.sum(y)\n",
    "Y.backward()\n",
    "derivative, =plt.plot(x.detach().numpy(),x.grad.detach().numpy(), label=\"relu'(x)\")\n",
    "anal_derivative = plt.plot(x.detach().numpy(), yprime(x).detach().numpy(), label=\"analytical sigmoid'(x)\", linestyle='--', linewidth=2)\n",
    "plt.legend()\n",
    "plt.show()\n",
    "plt.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Import data\n",
    "\n",
    "We will use simulated events corresponding to three physics processes.\n",
    "\n",
    "- ttH production\n",
    "- ttW production\n",
    "- Drell-Yan production\n",
    "\n",
    "We will select the multilepton final state, which is a challenging final state with a rich structure and nontrivial background separation.\n",
    "\n",
    "<img src=\"figs/2lss.png\" alt=\"ttH multilepton 2lss\" style=\"width:40%;\"/>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "INPUT_FOLDER = '/eos/user/c/cmsdas/2024/short-ex-mlg'\n",
    "sig = uproot.open(os.path.join(INPUT_FOLDER,'signal.root'))['Friends'].arrays(library=\"pd\")\n",
    "bk1 = uproot.open(os.path.join(INPUT_FOLDER,'background_1.root'))['Friends'].arrays(library=\"pd\")\n",
    "bk2 = uproot.open(os.path.join(INPUT_FOLDER,'background_2.root'))['Friends'].arrays(library=\"pd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data inspection\n",
    "\n",
    "We will now apply in one go all the manipulations of the input dataset that we have seen yesterday"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we drop all features that either correspond to unwanted objects (third lepton) or to labels we will need later on for regression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal = sig.drop([\"Hreco_Lep2_pt\", \"Hreco_Lep2_eta\", \"Hreco_Lep2_phi\", \"Hreco_Lep2_mass\", \"Hreco_evt_tag\", \"Hreco_HTXS_Higgs_pt\", \"Hreco_HTXS_Higgs_y\"], axis=1 )\n",
    "bkg1 = bk1.drop([\"Hreco_Lep2_pt\", \"Hreco_Lep2_eta\", \"Hreco_Lep2_phi\", \"Hreco_Lep2_mass\", \"Hreco_evt_tag\",\"Hreco_HTXS_Higgs_pt\", \"Hreco_HTXS_Higgs_y\"], axis=1 )\n",
    "bkg2 = bk2.drop([\"Hreco_Lep2_pt\", \"Hreco_Lep2_eta\", \"Hreco_Lep2_phi\", \"Hreco_Lep2_mass\", \"Hreco_evt_tag\",\"Hreco_HTXS_Higgs_pt\", \"Hreco_HTXS_Higgs_y\"], axis=1 )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we add the labels for classification..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal['label'] = 1\n",
    "bkg = pd.concat([bkg1, bkg2])\n",
    "bkg['label'] = 0\n",
    "data = pd.concat([signal,bkg]).sample(frac=1).reset_index(drop=True)\n",
    "X = data.drop([\"label\"], axis=1)\n",
    "y = data[\"label\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and we split the data into training and test dataset.\n",
    "Let's also go straight to the downsampling (you can run on your own on the whole training dataset, but for this demonstration we don't need to do that)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sklearn\n",
    "X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.33, random_state=42)\n",
    "print(f\"We have {len(X_train)} training samples with {sum(y_train)} signal and {sum(1-y_train)} background events\")\n",
    "print(f\"We have {len(X_test)} testing samples with {sum(y_test)} signal and {sum(1-y_test)} background events\")\n",
    "\n",
    "Ntrain=10000\n",
    "Ntest=2000\n",
    "X_train = X_train[:Ntrain]\n",
    "y_train = y_train[:Ntrain]\n",
    "X_test = X_test[:Ntest]\n",
    "y_test = y_test[:Ntest]\n",
    "\n",
    "# LABEL OF THIS PLACE HERE, WILL BE USEFUL LATER"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For neural networks we will use `pytorch`, a backend designed natively for tensor operations.\n",
    "I prefer it to tensorflow, because it exposes (i.e. you have to call them explicitly in your code) the optimizer steps and the backpropagation steps.\n",
    "\n",
    "You could also use the `tensorflow` backend, either directly or through the `keras` frontend.\n",
    "Saying \"I use keras\" does not tell you which backend is being used. It used to be either `tensorflow` or `theano`. Nowadays `keras` is I think almost embedded inside tensorflow, but it is still good to specify."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`torch` handles the data management via the `Dataset` and `DataLoader` classes.\n",
    "Here we don't need any specific `Dataset` class, because we are not doing sophisticated things, but you may need that in the future.\n",
    "\n",
    "The `Dataloader` class takes care of providing quick access to the data by sampling batches that are then fed to the network for (mini)batch gradient descent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MyDataset(Dataset):\n",
    "    def __init__(self, X, y, device=torch.device(\"cpu\")):\n",
    "        self.X = torch.Tensor(X.values if isinstance(X, pd.core.frame.DataFrame) else X).to(device)\n",
    "        self.y = torch.Tensor(y.values).to(device)\n",
    "\n",
    "    def __len__(self):\n",
    "        return len(self.y)\n",
    "\n",
    "    def __getitem__(self, idx):\n",
    "        label = self.y[idx]\n",
    "        datum = self.X[idx]\n",
    "        \n",
    "        return datum, label\n",
    "\n",
    "batch_size=512 # Minibatch learning\n",
    "\n",
    "\n",
    "train_dataset = MyDataset(X_train, y_train)\n",
    "test_dataset = MyDataset(X_test, y_test)\n",
    "\n",
    "train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)\n",
    "test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)\n",
    "\n",
    "train_features, train_labels = next(iter(train_dataloader))\n",
    "print(f\"Feature batch shape: {train_features.size()}\")\n",
    "print(f\"Labels batch shape: {train_labels.size()}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For educational purposes, let's get access the data loader via its iterator, and sample a single batch by calling `next` on the iterator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "random_batch_X, random_batch_y = next(iter(train_dataloader))\n",
    "print(random_batch_X.shape, random_batch_y.shape) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's build a simple neural network, by inheriting from the `nn.Module` class. **This is very crucial, because that class is the responsible for providing the automatic differentiation infrastructure for tracking parameters and performing backpropagation**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class NeuralNetwork(nn.Module):\n",
    "    def __init__(self, ninputs, device=torch.device(\"cpu\")):\n",
    "        super().__init__()\n",
    "        self.device = device\n",
    "        \n",
    "        self.linear_relu_stack = nn.Sequential(\n",
    "            nn.Linear(ninputs, 512),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(512, 128),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(128,64),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(64,8),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(8, 1),\n",
    "            nn.Sigmoid()\n",
    "        )\n",
    "        self.linear_relu_stack.to(device)\n",
    "\n",
    "    def forward(self, x):\n",
    "        # Pass data through conv1\n",
    "        x = self.linear_relu_stack(x)\n",
    "        return x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's instantiate the neural network and print some info on it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = NeuralNetwork(X_train.shape[1])\n",
    "\n",
    "print(model) # some basic info\n",
    "\n",
    "print(\"Now let's see some more detailed info by using the torchinfo package\")\n",
    "torchinfo.summary(model, input_size=(batch_size, X_train.shape[1])) # the input size is (batch size, number of features)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's introduce a crucial concept: `torch` lets you manage in which device you want to put your data and models, to optimize access at different stages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "device = torch.device(\"cpu\")\n",
    "\n",
    "if torch.backends.mps.is_available() and torch.backends.mps.is_built():\n",
    "    device = torch.device(\"mps\")\n",
    "if torch.cuda.is_available() and torch.cuda.device_count()>0:\n",
    "    device = torch.device(\"cuda\")\n",
    "    \n",
    "print (\"Available device: \",device)\n",
    "\n",
    "\n",
    "# Get a batch from the dataloader\n",
    "random_batch_X, random_batch_y = next(iter(train_dataloader))\n",
    "\n",
    "print(\"The original dataloader resides in\", random_batch_X.get_device())\n",
    "\n",
    "# Let's reinstantiate the dataset\n",
    "train_dataset = MyDataset(X_train, y_train, device=device)\n",
    "test_dataset = MyDataset(X_test, y_test, device=device)\n",
    "\n",
    "train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)\n",
    "test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)\n",
    "\n",
    "random_batch_X, random_batch_y = next(iter(train_dataloader))\n",
    "\n",
    "print(\"The new dataloader puts the batches in in\", random_batch_X.get_device())\n",
    "\n",
    "# Reinstantiate the model, on the chosen device\n",
    "model = NeuralNetwork(X_train.shape[1], device)\n",
    "\n",
    "#check if the NN can be evaluated some data; note: it has not been trained yet\n",
    "print (model(torch.tensor(X_train.values[:10],device=device)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have learned how load the data into the GPU, how to define and instantiate a model. Now we need to define a training loop.\n",
    "\n",
    "In `keras`, this is wrapped hidden into the `.fit()` method, which I think is bad because it hides the actual procedure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train_loop(dataloader, model, loss_fn, optimizer, scheduler, device):\n",
    "    size = len(dataloader.dataset)\n",
    "    losses=[] # Track the loss function\n",
    "    # Set the model to training mode - important for batch normalization and dropout layers\n",
    "    # Unnecessary in this situation but added for best practices\n",
    "    model.train()\n",
    "    #for batch, (X, y) in enumerate(dataloader):\n",
    "    for (X,y) in tqdm(dataloader):\n",
    "        # Reset gradients (to avoid their accumulation)\n",
    "        optimizer.zero_grad()\n",
    "        # Compute prediction and loss\n",
    "        pred = model(X)\n",
    "        #if (all_equal3(pred.detach().numpy())):\n",
    "        #    print(\"All equal!\")\n",
    "        loss = loss_fn(pred.squeeze(dim=1), y)\n",
    "        losses.append(loss.detach().cpu())\n",
    "        # Backpropagation\n",
    "        loss.backward()\n",
    "        optimizer.step()\n",
    "\n",
    "    scheduler.step()\n",
    "    return np.mean(losses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to define the loop that is run on the test dataset.\n",
    "\n",
    "**The test dataset is just used for evaluating the output of the model. No backpropagation is needed, therefore backpropagation must be switched off!!!**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_loop(dataloader, model, loss_fn, device):\n",
    "    losses=[] # Track the loss function\n",
    "    # Set the model to evaluation mode - important for batch normalization and dropout layers\n",
    "    # Unnecessary in this situation but added for best practices\n",
    "    model.eval()\n",
    "    size = len(dataloader.dataset)\n",
    "    num_batches = len(dataloader)\n",
    "    test_loss, correct = 0, 0\n",
    "\n",
    "    # Evaluating the model with torch.no_grad() ensures that no gradients are computed during test mode\n",
    "    # also serves to reduce unnecessary gradient computations and memory usage for tensors with requires_grad=True\n",
    "    with torch.no_grad():\n",
    "        #for X, y in dataloader:\n",
    "        for (X,y) in tqdm(dataloader):\n",
    "            pred = model(X)\n",
    "            loss = loss_fn(pred.squeeze(dim=1), y).item()\n",
    "            losses.append(loss)\n",
    "            test_loss += loss\n",
    "            #correct += (pred.argmax(1) == y).type(torch.float).sum().item()\n",
    "            \n",
    "    return np.mean(losses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now read to train this!\n",
    "At the moment we are trying to do classification. We will set our loss function to be the cross entropy.\n",
    "\n",
    "Torch provides the functionality to use generic functions as loss function. We will show an example one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "loss_fn = torch.nn.BCELoss()\n",
    "#loss_fn = torch.nn.CrossEntropyLoss()\n",
    "\n",
    "#loss_fn = torch.nn.CrossEntropyLoss(reduction='none')\n",
    "def CPloss(y_hat,y):\n",
    "    loss = torch.mean( y[:,0]*torch.pow( y_hat - y[:,1], 2))\n",
    "    #quad=-1,2\n",
    "    #lin=-2,1\n",
    "    #sm=-3,0\n",
    "    return loss\n",
    "# We would use this loss function in the same way as the other predefined loss functions:\n",
    "# loss_fn=CPloss\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time to define optimizer and scheduler, number of epochs, and finally to train!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "epochs=100\n",
    "learningRate = 0.01\n",
    "optimizer = torch.optim.SGD(model.parameters(), lr=learningRate)\n",
    "scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.9)\n",
    "\n",
    "train_losses=[]\n",
    "test_losses=[]\n",
    "for t in range(epochs):\n",
    "    print(f\"Epoch {t+1}\\n-------------------------------\")\n",
    "    train_loss=train_loop(train_dataloader, model, loss_fn, optimizer, scheduler, device)\n",
    "    test_loss=test_loop(test_dataloader, model, loss_fn, device)\n",
    "    train_losses.append(train_loss)\n",
    "    test_losses.append(test_loss)\n",
    "    print(\"Avg train loss\", train_loss, \", Avg test loss\", test_loss, \"Current learning rate\", scheduler.get_last_lr())\n",
    "print(\"Done!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(train_losses, label=\"Average training loss\")\n",
    "plt.plot(test_losses, label=\"Average test loss\")\n",
    "plt.xlabel(\"Epoch\")\n",
    "plt.ylabel(\"Loss\")\n",
    "plt.legend(loc=\"best\")\n",
    "plt.show()\n",
    "plt.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_rocs(scores_labels_names):\n",
    "    plt.figure()\n",
    "    for score, label, name  in scores_labels_names:\n",
    "        fpr, tpr, thresholds = roc_curve(label, score)\n",
    "        plt.plot(\n",
    "            fpr, tpr, \n",
    "            linewidth=2, \n",
    "            label=f\"{name} (AUC = {100.*auc(fpr, tpr): .2f} %)\"\n",
    "        )\n",
    "    plt.plot([0, 1], [0, 1], color=\"navy\", lw=2, linestyle=\"--\")\n",
    "    plt.grid()\n",
    "    plt.xlim([0.0, 1.0])\n",
    "    plt.ylim([0.0, 1.05])\n",
    "    plt.xlabel(\"False Positive Rate\")\n",
    "    plt.ylabel(\"True Positive Rate\")\n",
    "    plt.title(\"Receiver Operating Characteristic curve\")\n",
    "    plt.legend(loc=\"lower right\")\n",
    "    plt.show()\n",
    "    plt.close()\n",
    "\n",
    "plot_rocs([\n",
    "    (model(torch.tensor(X_train.to_numpy(),device=model.device)).numpy(force=True), y_train, \"Train\"), \n",
    "    (model(torch.tensor(X_test.to_numpy(),device=model.device)).numpy(force=True), y_test, \"Test\")  \n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## WHOOPS! The network is not learning anything!!!\n",
    "\n",
    "What can we do?\n",
    "\n",
    "Go back to that cell that had this text: `# LABEL OF THIS PLACE HERE, WILL BE USEFUL LATER`\n",
    "and add there the following lines:\n",
    "\n",
    "```\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "\n",
    "for column in X_train.columns:\n",
    "    scaler = StandardScaler().fit(X_train[column])\n",
    "    X_train[column] = scaler.transform(X_train[column])\n",
    "    X_test[column] = scaler.transform(X_test[column])\n",
    "```\n",
    "\n",
    "You could also use the basic syntax recommended by the documentation, as follows, but then you would be standardizing all the features to exacly the same mean. This may work for some data sets, but for this specific one it does not (it actually significantly worsens the performance---you can try ;) ).\n",
    "\n",
    "```\n",
    "scaler = StandardScaler().fit(X_train)\n",
    "\n",
    "X_train[X_train.columns] = scaler.transform(X_train[X_train.columns])\n",
    "X_test[X_test.columns] = scaler.transform(X_test[X_test.columns])\n",
    "```\n",
    "\n",
    "Rerun starting from that cell, and now check the new loss function evolution.\n",
    "\n",
    "#### Can you explain what is the effect of these lines and their effect on the gradient descent?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The end"
   ]
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
